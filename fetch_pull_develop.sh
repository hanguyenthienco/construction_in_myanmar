#!/bin/bash

GREEN='\033[1;32m'
RED='\033[1;31m'
BYellow='\033[1;33m'
NC='\033[0m' # No Color


git fetch
git pull
echo -e "$GREEN DONE FETCH + PULL FROM: $NC $RED DEVELOP $NC\n\n"
echo -e "$BYellow Please enter any key to continue ... $NC"
read $ENTER
